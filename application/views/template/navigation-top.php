<div class="header navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="navbar-inner">
		<div class="container-fluid">
			<!-- BEGIN LOGO -->
			<a class="brand" href="<?php echo site_url() ?>">
			<img src="<?php echo base_url() ?>assets/logo/logo.png" alt="logo" width="25px" style="vertical-align:bottom" />
			UNDIKNAS
			</a>
			<!-- END LOGO -->
			<!-- BEGIN TOP NAVIGATION MENU -->					
			<div class="navbar hor-menu hidden-phone hidden-tablet">
				<div class="navbar-inner">
					<ul class="nav">
						<li class="visible-phone visible-tablet">
							<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
							<form class="sidebar-search">
								<div class="input-box">
									<a href="javascript:;" class="remove"></a>
									<input type="text" placeholder="cari..." />            
									<input type="button" class="submit" value=" " />
								</div>
							</form>
							<!-- END RESPONSIVE QUICK SEARCH FORM -->
						</li>
						<?php
						foreach ($top_menu->result() as $value) {
							$slug = $value->slug;
							if($this->uri->segment(1) == $value->slug){
								$class = "active";
							}else{
								$class = '';
							}
							if($this->menu->get_by($value->id_menu, 1)->num_rows() > 0){
								$a = ' data-hover="dropdown" data-close-others="true" href="#"';
								$arrow = '<span class="arrow"></span>';
								$carret = '<b class="caret-out"></b>';
								foreach ($this->menu->get_by($value->id_menu, 1)->result() as $active_parent) {
									if($this->uri->segment(1) == $active_parent->slug){
										$class = "active";
									}
								}
							}else{
								$url = site_url($slug);
								$a = ' href="'.$url.'"';
								$arrow = '';
								$carret = '';
							}
							?>
							<li class="<?php echo $class ?>">
								<a <?php echo $a ?>>
									<?php echo $value->name ?> 
									<?php echo $arrow; ?>
									<?php
									if($class != ''){
										?>
										<span class="selected"></span>
										<?php
									}
									?>
								</a>
								<?php
								if($this->menu->get_by($value->id_menu, 1)->num_rows() > 0){
								?>
								<ul class="dropdown-menu">
								<?php
									foreach ($this->menu->get_by($value->id_menu, 1)->result() as $dropmenu) {
										?>
										<li class="<?php echo $active ?>">
											<a href="<?php echo site_url($dropmenu->slug) ?>"><?php echo $dropmenu->name ?></a>
										</li>
										<?php
									}
								?>	
								</ul>
								<?php
								echo $carret;
								}
								?>	
							</li>
							<?php
						}
						?>
					</ul>
				</div>
			</div>		
			<!-- END TOP NAVIGATION MENU -->	
		</div>
	</div>
	<!-- END TOP NAVIGATION BAR -->
</div>