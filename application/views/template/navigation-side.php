<div class="page-sidebar nav-collapse collapse">
	<!-- BEGIN SIDEBAR MENU -->        	
	<ul class="page-sidebar-menu hidden-phone hidden-tablet">
		<li>
			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			<div class="sidebar-toggler hidden-phone"></div>
			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
		</li>
		<li style="padding:10px"></li>
		<?php
			if (isset($page_active) && $page_active == "Dashboard") {
				$dashboard = "active";
			}else{
				$dashboard = "";
			}
			if (isset($page_active) && $page_active == $active) {
				$menu_dashboard = "active";
			}else{
				$menu_dashboard = "";
			}
		?>
		<li class="start <?php echo $dashboard ?>">
			<a href="<?php echo site_url("dashboard") ?>">
			<i class="icon-home"></i> 
			<span class="title">Dashboard</span>
			</a>
		</li>
		<li class="start <?php echo $menu_dashboard ?>">
			<a href="<?php echo site_url($active) ?>">
			<i class="icon-home"></i> 
			<span class="title"><?php echo $active ?></span>
			</a>
		</li>
		<?php
		foreach ($side_menu as $key => $value) {
			if (is_array($value)) {
				$sub = explode("|", $key);
				if($this->uri->segment(2) == $sub[1]){
					$class = "active";
				}else{
					$class = "";
				}
				?>
				<li class="<?php echo $class ?>">
					<a href="javascript:;">
					<i class="icon-bookmark-empty"></i> 
					<span class="title"><?php echo $sub[0] ?></span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub-menu">
						<?php
						foreach ($value as $k => $v) {
							if($this->uri->uri_string() == $k){
								$class = "active";
							}else{
								$class = "";
							}
							?>
							<li class="<?php echo $class ?>">
								<a href="<?php echo site_url($k) ?>"><?php echo $v ?></a>
							</li>
							<?php
						}
						?>
					</ul>
				</li>
				<?php
			}else{
				if($this->uri->uri_string() == $key){
					$class = "active";
				}else{
					$class = "";
				}
				?>
				<li class="<?php echo $class ?>">
					<a href="<?php echo site_url($key) ?>">
						<i class="icon-bar-chart"></i> 
						<span class="title"><?php echo $value ?></span>
					</a>
				</li>
				<?php	
			}
		}
		?>
		<li style="padding-bottom:50px"></li>
	</ul>
	<!-- END SIDEBAR MENU -->
</div>