<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Barang</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "inventori/barang/insert", "Tambah Barang"); ?>
			<?php echo $this->xm->print_button("inventori/barang/doPDF", "inventori/barang/doExcel"); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("inventori/barang/table"); ?>
		</div>
	</div>
</div>