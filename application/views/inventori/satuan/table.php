<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:50px;">No.</th>
			<th>Nama Satuan</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
				<td><?php echo ++$no; ?></td>
				<td><?php echo $list->nama_satuan ?></td>
				<td><?php echo $this->xm->link_icon("edit", "modal", "inventori/satuan/edit/$list->id_satuan", "Edit Satuan $list->nama_satuan") ?></td>
				<td><?php echo $this->xm->link_icon("delete", "modal", "inventori/satuan/do_delete/$list->id_satuan", "Hapus Satuan $list->nama_satuan") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>