<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Satuan</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "inventori/satuan/insert", "Tambah Satuan"); ?>
			<?php echo $this->xm->print_button("inventori/satuan/doPDF", "inventori/satuan/doExcel"); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("inventori/satuan/table"); ?>
		</div>
	</div>
</div>