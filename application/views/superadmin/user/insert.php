<style type="text/css">
    span.switch-small{
        padding-left: 5px;
        padding-right: 5px;
    }
</style>
<?php echo form_open("superadmin/user/do_insert", 'class="form-horizontal form-bordered ajax-handler"'); ?>
    <div class="form-button">
        <button type="submit" class="btn blue">Simpan</button>
    </div>
    <h4 class="form-section">Informasi User</h4>
    <div class="row-fluid">
        <div class="span12"> <!-- untuk membuat kolom span6 = 12/jumlah kolom -->
            <?php
                echo $this->xm->input_text("Username", "username","","","required");
                echo $this->xm->input_text("Email", "email","","","required");
                echo $this->xm->input_text("password", "password","","","required");
                echo $this->xm->input_text("Confirmasi Password", "conf-password","","","required");
                echo $this->xm->textarea("akses", "akses","","","required");
            ?>    
        </div>
    </div>    
    <h3 class="form-section">Hak Akses User</h3>
    <div class="row-fluid">
        <div class="span12"> <!-- untuk membuat kolom span6 = 12/jumlah kolom -->
             <div class="control-group">
                 <label class="control-label">Semua Akses Menu</label>
                <div class="controls">
                    <div id="all-switch" class="switch switch-small" data-on-label='Access All' data-off-label='Access All'>
                        <input type="checkbox" class="toggle all" id="all" checked="checked"/>
                    </div>
                    <div id="all-read-switch" class="switch switch-small" data-on-label='Read All' data-off-label='Read All' data-on="info">
                        <input type="checkbox" class="toggle all-read" id="all-read"/>
                    </div>
                    <div id="all-create-switch" class="switch switch-small" data-on-label='Create All' data-off-label='Create All' data-on="success">
                        <input type="checkbox" class="toggle all-create" id="all-create"/>
                    </div>
                    <div id="all-update-switch" class="switch switch-small" data-on-label='Update All' data-off-label='Update All' data-on="warning">
                        <input type="checkbox" class="toggle all-update" id="all-update"/>
                    </div>
                    <div id="all-delete-switch" class="switch switch-small" data-on-label='Delete All' data-off-label='Delete All' data-on="danger">
                        <input type="checkbox" class="toggle all-delete" id="all-delete"/>
                    </div>
                </div>
            </div>
            <ul class="nav nav-tabs">
                <?php
                $i = 0;
                foreach ($level1->result() as $tab) {
                    ?>
                    <li <?php echo ($i==0)? 'class="active"':'' ?>> 
                        <a href="#<?php echo $tab->slug ?>" data-toggle="tab"><?php echo $tab->name ?></a>
                    </li>
                    <?php
                    $i++;
                }
                ?>
            </ul>
            <div class="tab-content" style="min-height:500px">
                <?php
                $i = 0;
                foreach ($level1->result() as $tab) {
                    ?>
                    <div class="tab-pane <?php echo ($i==0)? 'active':'' ?>" id="<?php echo $tab->slug ?>">
                        <div class="control-group">
                        <label class="control-label">Semua Akses <?php echo $tab->name ?></label>
                        <div class="controls">
                            <div id="all-switch-<?php echo $tab->slug ?>" data-menu="<?php echo $tab->slug ?>" class="menu-switch switch-small" data-on-label='Access All' data-off-label='Access All'>
                                <input type="checkbox" class="toggle all"/>
                            </div>
                            <div id="all-read-switch-<?php echo $tab->slug ?>" data-menu="<?php echo $tab->slug ?>" class="menu-switch switch-small" data-on-label='Read All' data-off-label='Read All' data-on="info">
                                <input type="checkbox" class="toggle all-read"/>
                            </div>
                            <div id="all-create-switch-<?php echo $tab->slug ?>" data-menu="<?php echo $tab->slug ?>" class="menu-switch switch-small" data-on-label='Create All' data-off-label='Create All' data-on="success">
                                <input type="checkbox" class="toggle all-create"/>
                            </div>
                            <div id="all-update-switch-<?php echo $tab->slug ?>" data-menu="<?php echo $tab->slug ?>" class="menu-switch switch-small" data-on-label='Update All' data-off-label='Update All' data-on="warning">
                                <input type="checkbox" class="toggle all-update" data-menu="<?php echo $tab->slug ?>"/>
                            </div>
                            <div id="all-delete-switch-<?php echo $tab->slug ?>" data-menu="<?php echo $tab->slug ?>" class="menu-switch switch-small" data-on-label='Delete All' data-off-label='Delete All' data-on="danger">
                                <input type="checkbox" class="toggle all-delete"/>
                            </div>
                        </div>
                    </div>
                        <?php
                        $side_menu = $this->menu->get_side($tab->id_menu);
                        foreach ($side_menu as $key => $value) {
                            ?>
                            <?php
                            if (is_array($value)) {
                                $sub = explode("|", $key);
                                ?>
                                <h4 class="form-section"><?php echo $sub[0] ?></h4>
                                <?php
                                foreach ($value as $k => $v) {
                                    ?>
                                    <div class="control-group">
                                        <label class="control-label"><?php echo $v ?></label>
                                        <div class="controls">
                                            <div class="switch<?php echo $tab->slug ?> read-switch switch-small" data-on-label='Read' data-off-label='Read' data-on="info">
                                                <input type="checkbox" class="toggle read" name="<?php echo $tab->slug ?>-read" id="<?php echo $tab->slug ?>-read" value="<?php echo $k ?>"/>
                                            </div>
                                            <div class="switch<?php echo $tab->slug ?> create-switch switch-small" data-on-label='Create' data-off-label='Create' data-on="success">
                                                <input type="checkbox" class="toggle create" name="<?php echo $tab->slug ?>-create" id="<?php echo $tab->slug ?>-create" value="<?php echo $k ?>"/>
                                            </div>
                                            <div class="switch<?php echo $tab->slug ?> update-switch switch-small" data-on-label='Update' data-off-label='Update' data-on="warning">
                                                <input type="checkbox" class="toggle update" name="<?php echo $tab->slug ?>-update" id="<?php echo $tab->slug ?>-update" value="<?php echo $k ?>"/>
                                            </div>
                                            <div class="switch<?php echo $tab->slug ?> delete-switch switch-small" data-on-label='Delete' data-off-label='Delete' data-on="danger">
                                                <input type="checkbox" class="toggle delete" name="<?php echo $tab->slug ?>-delete" id="<?php echo $tab->slug ?>-delete" value="<?php echo $k ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }else{
                                ?>
                                <div class="control-group">
                                    <label class="control-label"><?php echo $value ?></label>
                                    <div class="controls">
                                        <div class="switch<?php echo $tab->slug ?> read-switch switch-small" data-on-label='Read' data-off-label='Read' data-on="info">
                                            <input type="checkbox" class="toggle read" name="read" id="read"/>
                                        </div>
                                        <div class="switch<?php echo $tab->slug ?> create-switch switch-small" data-on-label='Create' data-off-label='Create' data-on="success">
                                            <input type="checkbox" class="toggle create" name="create" id="create"/>
                                        </div>
                                        <div class="switch<?php echo $tab->slug ?> update-switch switch-small" data-on-label='Update' data-off-label='Update' data-on="warning">
                                            <input type="checkbox" class="toggle update" name="update" id="update"/>
                                        </div>
                                        <div class="switch<?php echo $tab->slug ?> delete-switch switch-small" data-on-label='Delete' data-off-label='Delete' data-on="danger">
                                            <input type="checkbox" class="toggle delete" name="delete" id="delete"/>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <?php
                    $i++;
                }
                ?>
            </div>    
        </div>
    </div>    
<?php echo form_close(); ?>
<script type="text/javascript">
    
    function cekAkses(){
        var values = $('.switch-on input:checkbox').map(function () {
              return this.value;
            }).get();
            $("#akses").val(values);
    }

    jQuery(document).ready(function($) {
        $('#all-switch').bootstrapSwitch();
        $('#all-read-switch').bootstrapSwitch();
        $('#all-create-switch').bootstrapSwitch();
        $('#all-update-switch').bootstrapSwitch();
        $('#all-delete-switch').bootstrapSwitch();
        $('.read-switch').bootstrapSwitch();
        $('.create-switch').bootstrapSwitch();
        $('.update-switch').bootstrapSwitch();
        $('.delete-switch').bootstrapSwitch();
        $('.menu-switch').bootstrapSwitch();
    });


    jQuery(document).ready(function($) {
        $('#all-switch').on('switch-change', function (e, data) {
            if($(this).bootstrapSwitch('status')){
                $('#all-read-switch').bootstrapSwitch('setState', true);
                $('#all-create-switch').bootstrapSwitch('setState', true);
                $('#all-update-switch').bootstrapSwitch('setState', true);
                $('#all-delete-switch').bootstrapSwitch('setState', true);
                
            }else{
                $('#all-read-switch').bootstrapSwitch('setState', false);
                $('#all-create-switch').bootstrapSwitch('setState', false);
                $('#all-update-switch').bootstrapSwitch('setState', false);
                $('#all-delete-switch').bootstrapSwitch('setState', false);
            }
            cekAkses();
        });
        $('#all-read-switch').on('switch-change', function (e, data) {
            if($(this).bootstrapSwitch('status')){
                $('.read-switch').bootstrapSwitch('setState', true);
            }else{
                $('.read-switch').bootstrapSwitch('setState', false);
            }
            cekAkses();
        });
        $('#all-create-switch').on('switch-change', function (e, data) {
            if($(this).bootstrapSwitch('status')){
                $('.create-switch').bootstrapSwitch('setState', true);
            }else{
                $('.create-switch').bootstrapSwitch('setState', false);
            }
            cekAkses();
        });
        $('#all-update-switch').on('switch-change', function (e, data) {
            if($(this).bootstrapSwitch('status')){
                $('.update-switch').bootstrapSwitch('setState', true);
            }else{
                $('.update-switch').bootstrapSwitch('setState', false);
            }
            cekAkses();
        });
        $('#all-delete-switch').on('switch-change', function (e, data) {
            if($(this).bootstrapSwitch('status')){
                $('.delete-switch').bootstrapSwitch('setState', true);
            }else{
                $('.delete-switch').bootstrapSwitch('setState', false);
            }
            cekAkses();
        });

        $('.menu-switch').on('switch-change', function (e, data) {
            var data_menu = $(this).attr("data-menu");
            if($(this).bootstrapSwitch('status')){
                $('.switch'+data_menu).bootstrapSwitch('setState', true);
            }else{
                $('.switch'+data_menu).bootstrapSwitch('setState', false);
            }
            cekAkses();
        });
    });

</script>