<div class="span12 fluid">
	<?php echo form_open("akademik/fakultas/do_edit", 'class="form-horizontal ajax-handler"'); ?>
       <div class="control-group">
          <label class="control-label">Kode Fakultas</label>
          <div class="controls input-icon">
             <input type="text" name="kode_fakultas" id="kode_fakultas" class="span10 m-wrap" value="<?php echo $data->kode_fakultas ?>" />
             <input type="hidden" name="id_fakultas" id="id_fakultas" class="span10 m-wrap" value="<?php echo $data->id_fakultas ?>" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Singkatan Fakultas</label>
          <div class="controls input-icon">
             <input type="text" name="singkatan" id="singkatan" class="span10 m-wrap" value="<?php echo $data->singkatan ?>" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Nama Fakultas</label>
          <div class="controls input-icon">
             <input type="text" name="nama_fakultas" id="nama_fakultas" class="span10 m-wrap" value="<?php echo $data->nama_fakultas ?>" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">SK no.</label>
          <div class="controls input-icon">
             <input type="text" name="sk" id="sk" class="span10 m-wrap" value="<?php echo $data->sk ?>" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Nama Dekan</label>
          <div class="controls input-icon">
             <input type="text" name="dekan" id="dekan" class="span10 m-wrap" value="<?php echo $data->dekan ?>" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Pembantu Dekan 1</label>
          <div class="controls input-icon">
             <input type="text" name="pd1" id="pd1" class="span10 m-wrap" value="<?php echo $data->pd1 ?>" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Pembantu Dekan 2</label>
          <div class="controls input-icon">
             <input type="text" name="pd2" id="pd2" class="span10 m-wrap" value="<?php echo $data->pd2 ?>" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Pembantu Dekan 3</label>
          <div class="controls input-icon">
             <input type="text" name="pd3" id="pd3" class="span10 m-wrap" value="<?php echo $data->pd3 ?>" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Tahun Kurikulum</label>
          <div class="controls input-icon">
             <input type="text" name="kurikulum" id="kurikulum" class="span10 m-wrap" value="<?php echo $data->kurikulum ?>" />
          </div>
       </div>
       <div class="form-actions">
          <button type="submit" class="btn blue">Simpan</button>
          <button type="button" data-dismiss="modal" class="btn">batal</button>
       </div>
    </form>
</div>
<script type="text/javascript">
	$("form").xmFormAjax();
</script>