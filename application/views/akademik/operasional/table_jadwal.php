<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:40px;">No.</th>
			<th>Nama Ruangan</th>
			<th>Kapasitas</th>
			<th>Fakultas</th>
			<th>Keterangan</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->nama_ruang ?></td>
			<td><?php echo $list->kapasitas ?></td>
			<td><?php echo $list->nama_fakultas ?></td>
			<td><?php echo $list->keterangan ?></td>
			<td><?php echo $this->xm->link_icon("detail", "modal", "akademik/ruang_kelas/detail/$list->id_ruang_kelas", "Detail $list->nama_ruang") ?></td>
			<td><?php echo $this->xm->link_icon("edit", "modal", "akademik/ruang_kelas/edit/$list->id_ruang_kelas", "Edit $list->nama_ruang") ?></td>
			<td><?php echo $this->xm->link_icon("delete", "modal", "akademik/ruang_kelas/do_delete/$list->id_ruang_kelas", "$list->nama_ruang") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>