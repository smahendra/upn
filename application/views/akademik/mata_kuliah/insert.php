	<?php echo form_open("akademik/mata_kuliah/do_insert", 'class="form-horizontal ajax-handler"'); ?>
        <div class="form-button">
              <button type="submit" class="btn blue">Simpan</button>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php
                    echo $this->xm->input_text("Kode MK", "kode_mk");
                    echo $this->xm->input_text("Nama MK Indonesia", "ina_mk");
                    echo $this->xm->input_text("Nama MK English", "en_mk");
                    echo $this->xm->input_text("Kurikulum", "kurikulum");
                    echo $this->xm->select_group("Program Studi", "id_prodi", "id_jurusan|nama_jurusan", $prodi, "Pilih Program Studi", "id_prodi|nama_prodi", "");
                ?>
            </div>
            <div class="span6">
                <?php
                    echo $this->xm->select("Kategori MK", "id_kategori",  $kategori, "Pilih Kategori", "id_kategori_mk|singkatan");
                    $option_sks = array("1|1 SKS", "2|2 SKS", "3|3 SKS", "4|4 SKS", "5|5 SKS", "6|6 SKS");
                    echo $this->xm->select("Jumlah SKS", "sks", $option_sks ,"Pilih SKS");
                    $option_semester = array("1", "2", "3", "4", "5", "6", "7", "8");
                    echo $this->xm->select("Semester", "semester", $option_semester ,"Pilih Semester");
                    echo $this->xm->select_group("Syarat Mata Kuliah", "syarat_mk", "semester|semester", $order_mk, "Pilih Syarat Mata Kuliah", "id_mata_kuliah|nama", "Semester");
                    echo $this->xm->input_text("Syarat SKS", "syarat_sks");
                ?>                
            </div>
        </div>    
    <?php echo form_close(); ?>