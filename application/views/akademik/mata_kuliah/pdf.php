<link href="<?php echo base_url();?>assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<?php echo $this->xm->table_open() ?>
	<thead>
		<tr>
			<th style="width:8px;">No.</th>
			<th>Kode MK</th>
			<th>Nama MK</th>
			<th>SKS</th>
			<th>Semester</th>
			<th>Program Studi</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->kode ?></td>
			<td><?php echo $list->nama ?></td>
			<td><?php echo $list->sks ?></td>
			<td><?php echo $list->semester ?></td>
			<td><?php echo $list->nama_prodi ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>