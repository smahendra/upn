<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:50px;">No.</th>
			<th>Nama Agama</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
				<td><?php echo ++$no; ?></td>
				<td><?php echo $list->nama_agama ?></td>
				<td><?php echo $this->xm->link_icon("edit", "modal", "akademik/agama/edit/$list->id_agama", "Edit Agama $list->nama_agama") ?></td>
				<td><?php echo $this->xm->link_icon("delete", "modal", "akademik/agama/do_delete/$list->id_agama", "Hapus Agama $list->nama_agama") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>