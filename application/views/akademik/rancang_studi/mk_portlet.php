<div class="row-fluid">
	<div class="well">
	<?php	
		echo $this->xm->form_text();
		echo $this->xm->text("Nama", $mahasiswa->nama);
		echo $this->xm->text("NIM", $mahasiswa->nim);
		echo $this->xm->close_form_text();
	?>	
	</div>
</div>
<div class="row-fluid">
	<div class="span6">
		<div class="portlet box blue" id="portlet-mk-list">
			<div class="portlet-title">
				<div class="caption"><i class="icon-reorder"></i>Mata Kuliah yang ditawarkan</div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn dropdown-toggle blue" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" href="#">
							Paket MK <i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a class="semester-mk" href="">Semua Mata Kuliah</a></li>
							<li><a class="semester-mk" href="1">Semester 1</a></li>
							<li><a class="semester-mk" href="2">Semester 2</a></li>
							<li><a class="semester-mk" href="3">Semester 3</a></li>
							<li><a class="semester-mk" href="4">Semester 4</a></li>
							<li><a class="semester-mk" href="5">Semester 5</a></li>
							<li><a class="semester-mk" href="6">Semester 6</a></li>
							<li><a class="semester-mk" href="7">Semester 7</a></li>
							<li><a class="semester-mk" href="8">Semester 8</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div style="height:500px; overflow:scroll" id="table_mk_list">
					<?php $this->load->view("akademik/rancang_studi/mk_table") ?>
				</div>
			</div>
		</div>
	</div>
	<div class="span6">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="icon-reorder"></i>Rancangan Studi Mahasiswa</div>
				<div class="actions">
					<button class="btn blue" id="save-studi">Simpan</button>
				</div>
			</div>
			<div class="portlet-body">
				<div style="height:500px; overflow:scroll">
					<table id="mk_mahasiswa" class="table table-condensed table-hover">
						<tbody>
							<?php
							foreach ($mahasiswa_mk->result() as $list) {
							?>
							<tr id="tr<?php echo $list->id_mata_kuliah ?>">
								<td>
									<label class="checkbox">
										<input class="checkmhs" type="checkbox" value="<?php echo $list->id_mata_kuliah ?>" id="<?php echo $list->id_mata_kuliah ?>" data-sks="<?php echo $list->sks ?>" name="<?php echo $list->id_mata_kuliah ?>" syarat-sks="<?php echo $list->syarat_sks ?>" syarat-mk="<?php echo $list->syarat_mk ?>" checked>
										<?php echo $list->nama_mk ?>
									</label>
								</td>
								<td style="width:35px; text-align:right">
									<span class="badge badge-success"><?php echo $list->sks ?> SKS</span>
								</td>
								<td class="need-remove" style="width: 30px; display: none;">
									<a class="btn mini green-stripe transfer"><i class="icon-arrow-right"></i></a>
								</td>
								<td style="width:30px">
									<a class="btn mini red-stripe transfer-cancel"><i class="icon-remove"></i></a>
								</td>
							</tr>
							<?php
							}
							?>
						</tbody>
						<tfoot>
							<tr>
								<th>Total SKS</th>
								<th colspan="2">
									<span class="badge badge-important">
										<span id="total-sks"></span> SKS
									</span>
								</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<input type="hidden" id="mklist">
			<input type="hidden" id="mkmahasiswa">
			<input type="hidden" id="max-sks" value="<?php echo $max_sks ?>">
			<input type="hidden" id="id-mahasiswa" value="<?php echo $id_mahasiswa ?>">
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#save-studi").on("click", function(){
		var mkmahasiswa 	= $("#mkmahasiswa").val();
		var id_mahasiswa 	= $("#id-mahasiswa").val();
		var tahun_ajaran 	= $("#tahun_ajaran").val();

		jQuery.ajax({
			url:'<?php echo site_url("akademik/rancang_studi/do_create_rancang_studi") ?>',
			type: 'post',
			data:{
				studi_mahasiswa: mkmahasiswa,
				id_mahasiswa: id_mahasiswa,
				tahun_ajaran: tahun_ajaran
			},
			success: function(res){
				statusNote("Rancangan Studi semester mahasiswa tersimpan!");
			},
			error: function(){
				alert("some error");
			}
		});					
	});

$(".transfer-cancel").click(function(e){
					e.preventDefault();
					var element = $(this).parent().parent();
					var id      = element.attr("id");
					$("#mk_list tbody tr#"+id).show();
					element.remove();
					cekMKMahasiswaChecked();
				});
</script>