<table class="table table-condensed table-hover" id="mk_list">
	<tbody>
		<?php
		$i = 0;
		$semester = 0;
		foreach ($list_mk->result() as $mk) {
			?>
			<tr class="tr-mk" id="tr<?php echo $mk->id_mata_kuliah ?>">
				<td>
					<label class="checkbox">
								<input class="checkmk" type="checkbox" value="<?php echo $mk->id_mata_kuliah ?>" id="<?php echo $mk->id_mata_kuliah ?>" data-sks="<?php echo $mk->sks ?>" name="<?php echo $mk->id_mata_kuliah ?>" syarat-sks="<?php echo $mk->syarat_sks ?>" syarat-mk="<?php echo $mk->syarat_mk ?>"/>
						<?php echo $mk->nama ?>
					</label>
				</td>
				<td style="width:35px; text-align:right;">
					<span class="badge badge-success"><?php echo $mk->sks ?> SKS</span>
				</td>
				<td class="need-remove" style="width:30px">
					<a class="btn mini green-stripe transfer"><i class="icon-arrow-right"></i></a>
				</td>
			</tr>
		<?php	
		}
		?>
	</tbody>
</table>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		
		$(".checkmk").on("change",function(e){
			cekMKChecked();
		});

		$(".semester-mk").on("click",function(e){
			e.preventDefault();
			$("#initiate-loading-page").show();
			var id_fakultas  = $("#id_fakultas").val();
			var id_mahasiswa = $("#id_mahasiswa").val();
			var semester_mk  = $(this).attr("href");
			var nama_value = $(this).html();
			
			jQuery.ajax({
	        	url:'<?php echo site_url("akademik/rancang_studi/do_select_semester") ?>',
		        type:"post",
		        data:{	id_fakultas: id_fakultas,
		        		id_mahasiswa: id_mahasiswa,
		        		semester_mk: semester_mk
		        	},
		        success:function(result){
		        	$("#table_mk_list").html(result);
		        	$("#portlet-mk-list .caption").html('<i class="icon-reorder"></i>'+nama_value);
		        	cekMKMahasiswaChecked();
		        	$("#initiate-loading-page").hide();
		        },
		        error:function(res){
		          	alert("form ajax portlet error");
		          	$("#initiate-loading-page").hide();
		        }
	      	});
		});
		

		$(".transfer").click(function(e){
			e.preventDefault();
			var max_sks     = $("#max-sks").val();
			var element     = $(this).parent().parent();
			var id          = element.attr("id");
			var element_sks = $("#mk_list tbody tr#"+id + " input.checkmk").attr("data-sks");
			var syarat_sks  = $("#mk_list tbody tr#"+id + " input.checkmk").attr("syarat-sks");
			var syarat_mk   = $("#mk_list tbody tr#"+id + " input.checkmk").attr("syarat-mk");
			var maxSks = cekMaxSks(element_sks);
			var syarat = cekSyaratMk(syarat_sks, syarat_mk);
			if(maxSks == true && syarat == true){
				$("tr#"+id + " input.checkmk").prop("checked", false);
				element.clone().appendTo("#mk_mahasiswa tbody");
				$("#mk_mahasiswa tbody tr#"+id + " input.checkmk").addClass("checkmhs");
				$("#mk_mahasiswa tbody tr#"+id + " input.checkmhs").removeClass("checkmk");
				$("#mk_mahasiswa tbody .checkmhs").prop('checked', true);
				$("#mk_mahasiswa tbody tr#"+id + " .need-remove").hide();
				$("#mk_mahasiswa tbody tr#"+id).append('<td style="width:30px"><a class="btn mini red-stripe transfer-cancel"><i class="icon-remove"></i></a></td>');
				//element.hide();
				cekMKMahasiswaChecked();
				$(".transfer-cancel").click(function(e){
					e.preventDefault();
					var element = $(this).parent().parent();
					var id      = element.attr("id");
					$("#mk_list tbody tr#"+id).show('fade');
					element.hide('fade', function() {
						element.remove();	
						cekMKMahasiswaChecked();
					});
				});
			}else{
				if(maxSks == false){
					$("#alert-modal .modal-body").html("<p>Maksimal SKS yang boleh diambil : "+max_sks+" SKS</p>");
					$("#alert-modal").modal("show");
				}else if( syarat == false){
					$("#alert-modal .modal-body").html("<p>Mahasiswa tidak memenuhi syarat untuk mengambil mata kuliah ini!</p>");
					$("#alert-modal").modal("show");
				}else{
					alert(maxSks+"error"+syarat);
				}
			}
		});
	});
</script>