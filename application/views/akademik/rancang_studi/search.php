	<?php echo form_open("akademik/rancang_studi/do_search", 'class="form-vertical ajax-portlet"'); ?>
	   <div class="span3">
	      <label class="control-label">Fakultas</label>
	      <div class="controls input-icon">
	         <select class="chosen on-change" elemen-affected="#id_mahasiswa" data-url="<?php echo base_url() ?>akademik/rancang_studi/do_select_fakultas" concat-text="nama" concat-text2="nim" concat-value="id_mahasiswa" data-placeholder="Pilih Fakultas" tabindex="1" name="id_fakultas" id="id_fakultas">
	         	 <option value="">Pilih Fakultas</option>
	            <?php 
	            foreach ($fakultas->result() as $row) {
	              ?>
	                <option value="<?php echo $row->id_fakultas ?>"><?php echo $row->nama_fakultas ?></option>                                    
	            <?php }?>
	         </select>
	       </div>
	   </div>
	   <div class="span3">
	      <label class="control-label">Mahasiswa</label>
	      <div class="controls input-icon">
	         <select class="chosen" data-placeholder="Pilih Mahasiswa" tabindex="1" name="id_mahasiswa" id="id_mahasiswa">
	            <option value="">Pilih Mahasiswa</option>
	         </select>
	       </div>
	   	</div>
       	<div class="span5">
          	<label class="control-label">Tahun ajaran</label>
          	<div class="controls input-icon">
				<select class="chosen" data-placeholder="Tahun ajaran" tabindex="1" name="tahun_ajaran" id="tahun_ajaran">
					<option value="<?php echo $tahun_ajaran->id_tahun_ajaran ?>">
						<?php echo $tahun_ajaran->tahun_ajaran." (".$tahun_ajaran->periode.")" ?>
					</option>
				</select>
      		</div>
       	</div>
       	<div class="span1">
          	<label class="control-label">&nbsp;</label>
          	<div class="controls input-icon">
	          	<button type="submit" class="btn blue">Cari</button>
          	</div>
       	</div>
       	<input type="hidden" name="periode" id="periode" value="<?php echo $tahun_ajaran->periode ?>">       
	<?php echo form_close() ?>
