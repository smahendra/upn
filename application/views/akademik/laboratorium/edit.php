	<?php echo form_open("akademik/laboratorium/do_edit", 'class="form-horizontal ajax-handler"'); ?>
        <div class="form-button">
            <button type="submit" class="btn blue">Simpan</button>
            <input type="hidden" name="id_laboratorium" id="id_laboratorium" value="<?php echo $data->id_laboratorium ?>" />
        </div>
        <div class="row-fluid">
            <?php
                echo $this->xm->input_text("Nama Laboratorium", "nama_laboratorium", $data->nama_laboratorium);
                echo $this->xm->select("Ruangan", "ruang", $ruangan, "Hapus", "id_ruang_kelas|nama_ruang", $data->ruang);
                echo $this->xm->select("Ketua Laboratorium", "ketua", $pegawai, "Hapus", "id_pegawai|full_name", $data->ketua);
            ?>
        </div>   
    <?php echo form_close(); ?>