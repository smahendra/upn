	<?php echo form_open("akademik/laboratorium/do_insert", 'class="form-horizontal ajax-handler"'); ?>
        <div class="form-button">
            <button type="submit" class="btn blue">Simpan</button>
        </div>
        <div class="row-fluid">
            <?php
                echo $this->xm->input_text("Nama Laboratorium", "nama_laboratorium");
                echo $this->xm->select("Ruangan", "ruang", $ruangan, "Pilih Ruangan", "id_ruang_kelas|nama_ruang");
                echo $this->xm->select("Ketua Laboratorium", "ketua", $pegawai, "Pilih Ketua Laboratorium", "id_pegawai|full_name");
            ?>
        </div>    
    <?php echo form_close(); ?>