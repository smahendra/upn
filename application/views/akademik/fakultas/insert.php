	<?php echo form_open("akademik/fakultas/do_insert", 'class="form-horizontal ajax-handler"'); ?>
        <div class="form-button">
            <button type="submit" class="btn blue">Simpan</button>
        </div>
        <div class="row-fluid">
            <div class="span6"> <!-- untuk membuat kolom span6 = 12/jumlah kolom -->
                <?php
                    echo $this->xm->input_text("Kode Fakultas", "kode_fakultas","","","required");
                    echo $this->xm->input_text("Singkatan Fakultas", "singkatan","","","required");
                    echo $this->xm->input_text("Nama Fakultas", "nama_fakultas","","","required");
                    echo $this->xm->input_text("Tahun Kurikulum", "kurikulum","","","required");
                    $option = array("Ya", "Tidak");
                    echo $this->xm->select("Genap/Ganjil", "genap_ganjil", $option, "Pilih Genap / Ganjil","","","","required");
                ?>
            </div>
            <div class="span6">
                <?php
                    echo $this->xm->select("Nama Dekan", "dekan", $pegawai, "Pilih Dekan", "id_pegawai|full_name","","","required");
                    echo $this->xm->select("Wakil Dekan", "wakil_dekan", $pegawai, "Pilih Wakil Dekan", "id_pegawai|full_name","","","required");
                    echo $this->xm->select("Pembantu Dekan I", "pd1", $pegawai, "Pilih Pembantu Dekan I", "id_pegawai|full_name","","","required");
                    echo $this->xm->select("Pembantu Dekan II", "pd2", $pegawai, "Pilih Pembantu Dekan II", "id_pegawai|full_name","","","required");
                    echo $this->xm->select("Pembantu Dekan III", "pd3", $pegawai, "Pilih Pembantu Dekan III", "id_pegawai|full_name","","","required");
                ?>
            </div> 
        </div>
    <?php echo form_close(); ?>