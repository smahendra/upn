	<?php echo form_open("akademik/khs/do_khs_search", 'class="form-vertical"'); ?>
	   <div class="span3">
	      <label class="control-label">Fakultas</label>
	      <div class="controls input-icon">
	         <select class="chosen on-change" elemen-affected="#id_jurusan" data-url="<?php echo base_url() ?>akademik/khs/do_select_jurusan" concat-text="nama_jurusan" concat-text2="" concat-value="id_jurusan" data-placeholder="Pilih Fakultas" tabindex="1" name="id_fakultas" id="id_fakultas">
	            <option value="">Pilih Fakultas</option>
	            <?php 
	            foreach ($fakultas->result() as $row) {
	              ?>
	                <option value="<?php echo $row->id_fakultas ?>"><?php echo $row->nama_fakultas ?></option>                                    
	            <?php }?>
	         </select>
	       </div>
	   </div>
	   <div class="span3">
	      <label class="control-label">Jurusan</label>
	      <div class="controls input-icon">
	         <select class="chosen" data-placeholder="Pilih Jurusan" tabindex="1" name="id_jurusan" id="id_jurusan">
	            
	         </select>
	       </div>
	   </div>
       <div class="span3">
          <label class="control-label">Semester Ditempuh</label>
          <div class="controls input-icon">
              <select class="chosen" data-placeholder="Pilih Semester" tabindex="1" name="semester" id="semester">
                <option value=""></option>
                <option value="1">Semester 1</option>
                <option value="2">Semester 2</option>
                <option value="3">Semester 3</option>
                <option value="4">Semester 4</option>
                <option value="5">Semester 5</option>
                <option value="6">Semester 6</option>
                <option value="7">Semester 7</option>
                <option value="8">Semester 8</option>
                <option value="8">Semester 9</option>
                <option value="8">Semester 10</option>
                <option value="8">Semester 11</option>
                <option value="8">Semester 12</option>
                <option value="8">Semester 13</option>
                <option value="8">Semester 14</option>
             </select>
          </div>
       </div>
       <div class="span3">
          <label class="control-label">&nbsp;</label>
          <div class="controls input-icon">
	          <button type="submit" class="btn blue">Cari</button>
          </div>
       </div>       
	<?php echo form_close() ?>
