<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:40px;">No.</th>
			<th>Kode Jurusan</th>
			<th>Nama Jurusan</th>
			<th>Ketua Jurusan</th>
			<th>Sekretaris Jurusan</th>
			<th>Fakultas</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->kodejurusan ?></td>
			<td><?php echo $list->nama_jurusan ?></td>
			<td><?php echo $list->nama_ketua ?></td>
			<td><?php echo $list->nama_sekretaris ?></td>
			<td><?php echo $list->nama_fakultas ?></td>
			<td><?php echo $this->xm->link_icon("detail", "modal", "akademik/jurusan/detail/$list->id_jurusan", "Detail Jurusan $list->nama_jurusan") ?></td>
			<td><?php echo $this->xm->link_icon("edit", "modal", "akademik/jurusan/edit/$list->id_jurusan", "Edit Jurusan $list->nama_jurusan") ?></td>
			<td><?php echo $this->xm->link_icon("delete", "modal", "akademik/jurusan/do_delete/$list->id_jurusan", "$list->nama_jurusan") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>