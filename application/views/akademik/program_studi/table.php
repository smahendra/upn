<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:30px;">No.</th>
			<th>Kode</th>
			<th>Prodi</th>
			<th>SK BAN-PT</th>
			<th>SK Operasional</th>
			<th>Fakultas</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->kodeprodi ?></td>
			<td><?php echo $list->nama_prodi ?></td>
			<td><?php echo $list->sk ?></td>
			<td><?php echo $list->sk_ijin ?></td>
			<td><?php echo $list->nama_fakultas ?></td>
			<td><?php echo $this->xm->link_icon("detail", "modal","akademik/program_studi/detail/$list->id_prodi", "Detail $list->nama_prodi") ?></td>
			<td><?php echo $this->xm->link_icon("edit", "modal","akademik/program_studi/edit/$list->id_prodi", "Edit $list->nama_prodi") ?></td>
			<td><?php echo $this->xm->link_icon("delete", "modal","akademik/program_studi/do_delete/$list->id_prodi", "$list->nama_prodi") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>