<div class="portlet box blue">
	<div class="portlet-title">
	  	<div class="caption"><i class="icon-reorder"></i>Data Program Studi</div>
		<div class="actions">
			<?php echo $this->xm->button("insert","modal","akademik/program_studi/insert", "Tambah Program Studi"); ?>
            <?php echo $this->xm->print_button("akademik/program_studi/doPDF", "akademik/program_studi/doExcel"); ?>
		</div>				
    </div>
    <div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("akademik/program_studi/table"); ?>
		</div>
	</div>
</div>
