<?php echo form_open("akademik/kategori_mk/do_insert", 'class="form-horizontal ajax-handler"'); ?>
    <div class="form-button">
        <button type="submit" class="btn blue">Simpan</button>
    </div>
    <div class="row-fluid">
        <?php
            echo $this->xm->input_text("Nama Kategori", "nama_kategori");
            echo $this->xm->input_text("Singkatan Kategori", "singkatan");
            echo $this->xm->textarea("Keterangan", "keterangan");
        ?>
    </div>      
<?php echo form_close(); ?>