<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:8px;">No.</th>
			<th>Kuitansi #</th>
			<th>Nama Maru</th>
			<th>Email</th>
			<th style="width:8px;">Gelombang</th>
			<th>Thn Ajaran</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->no_kuitansi ?></td>
			<td><?php echo $list->nama ?></td>
			<td><?php echo $list->email ?></td>
			<td><?php echo $list->gelombang ?></td>
			<td><?php echo $list->tahun_ajaran ?></td>
			<td><?php echo $this->xm->link_icon("detail", "modal", "akademik/registrasi/maru_detail/$list->id_calon_mahasiswa", "Detail $list->nama") ?></td>
			<td><?php echo $this->xm->link_icon("edit","modal","akademik/registrasi/maru_form/$list->id_calon_mahasiswa", "$list->nama") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>