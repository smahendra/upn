<div class="row-fluid">
	<div class="span12">
		<div class="portlet box yellow">
			<div class="portlet-title">
				<h4><i class="icon-reorder"></i>Data Perangkat & Dosen</h4>
				<div class="tools">
					<a href="javascript:;" class="reload"></a>
					<a href="#portlet-config" data-toggle="modal" class="config"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="clearfix">
					<div class="btn-group">
						<?php echo $this->xtromedia->button_insert_modal("akademik/perangkat_dosen/insert", "Tambah Perangkat & Dosen"); ?>
					</div>
					<div class="btn-group pull-right">
						<button class="btn dropdown-toggle" data-toggle="dropdown">Print <i class="icon-angle-down"></i>
						</button>
						<ul class="dropdown-menu">
							<li><a href="#">Save as PDF</a></li>
							<li><a href="#">Export to Excel</a></li>
						</ul>
					</div>
				</div>
				<div class="table-container">
					<?php $this->load->view("akademik/perangkat_dosen/table"); ?>
				</div>
			</div>
		</div>
	</div>
</div>