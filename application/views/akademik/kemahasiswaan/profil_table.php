<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:40px;">No.</th>
			<th>NIM</th>
			<th>Nama Mahasiswa</th>
			<th>Email</th>
			<th>Status Akademik</th>
			<th>Tahun Ajaran</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->nim ?></td>
			<td><?php echo $list->nama ?></td>
			<td><?php echo $list->email ?></td>
			<td><?php echo $list->status_akademik ?></td>
			<td><?php echo $list->tahun_ajaran ?></td>
			<td><?php echo $this->xm->link_icon("edit","modal","akademik/kemahasiswaan/pengajuan_cuti/$list->id_mahasiswa", "Form Cuti $list->nama ($list->nim)") ?></td>
			<td><?php echo $this->xm->link_icon("edit","modal","akademik/kemahasiswaan/pengajuan_dropout/$list->id_mahasiswa", "Form Dropout $list->nama ($list->nim)") ?></td>
			<td><?php echo $this->xm->link_icon("detail","modal","akademik/kemahasiswaan/profil_detail/$list->id_mahasiswa", "Detail $list->nama") ?></td>
			<td><?php echo $this->xm->link_icon("edit","modal","akademik/kemahasiswaan/profil_edit/$list->id_mahasiswa", "$list->nama") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>