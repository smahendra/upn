<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Profil Mahasiswa</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("akademik/kemahasiswaan/profil_table"); ?>
		</div>
	</div>
</div>
