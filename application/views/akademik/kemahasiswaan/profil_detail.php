<?php 
echo $this->xm->form_text();
    echo $this->xm->text("NIM", $data->nim);
    echo $this->xm->text("Nama Mahasiswa", $data->nama);
    echo $this->xm->text("Jenis Kelamin", $data->kelamin);
    echo $this->xm->text("Email", $data->email);
    echo $this->xm->text("Alamat / Kode Pos", $data->alamat.' / '.$data->kodepos);
    echo $this->xm->text("Telepon", $data->telepon);
    echo $this->xm->text("Agama", $data->nama_agama);
    echo $this->xm->text("Kewarganegaraan", $data->warga);
    echo $this->xm->text("Pekerjaan", $data->kerja);
    echo $this->xm->text("Asal / Jurusan / Tahun Tamat Sekolah", $data->sekolah_asal.' - '.$data->kabupaten.' / '.$data->jurusan_sekolah.' / '.$data->tahun_tamat);
    echo $this->xm->text("No / Tanggal Ijasah", $data->no_ijasah.' / '.$this->xm->format_tanggal($data->tgl_ijasah, "d M Y"));
    echo $this->xm->text("Fakultas / Jurusan / Prodi", $data->nama_fakultas.' / '.$data->nama_jurusan.' / '.$data->nama_prodi);
    echo $this->xm->text("Nama Orang Tua", $data->nama_ortu);
    echo $this->xm->text("Pekerjaan Orang Tua", $data->pekerjaan_ortu);
    echo $this->xm->text("Alamat / Kode Pos Orang Tua", $data->alamat_ortu.' / '.$data->kodepos_ortu);
    echo $this->xm->text("Telepon Orang Tua", $data->telepon_ortu);
    echo $this->xm->text("Terakhir Diupdate", $this->xm->format_tanggal($data->modified, "d M Y"));
echo $this->xm->close_form_text(); 
?>