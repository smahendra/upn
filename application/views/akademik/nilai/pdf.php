<link href="<?php echo base_url();?>assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<?php echo $this->xm->table_open() ?>
	<thead>
		<tr>
			<th style="width:8px;">No.</th>
			<th>Initial</th>
			<th>Index</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->initial ?></td>
			<td><?php echo $list->index ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>