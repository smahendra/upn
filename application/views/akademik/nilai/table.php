<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:40px;">No.</th>
			<th>Initial</th>
			<th>Index</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->initial ?></td>
			<td><?php echo $list->index ?></td>
			<td><?php echo $this->xm->link_icon("edit", "modal", "akademik/nilai/edit/$list->id_nilai", "Edit Nilai $list->initial") ?></td>
			<td><?php echo $this->xm->link_icon("delete", "modal", "akademik/nilai/do_delete/$list->id_nilai", "$list->initial") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>