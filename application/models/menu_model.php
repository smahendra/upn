<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_menu", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$this->db->order_by("order", "asc");
		$menu = $this->db->get("menu");
		return $menu;
	}

	function get_all(){
		$menu = $this->db->get("menu");
		return $menu;
	}

	function get_by($parent='0', $level='1'){
		$this->db->where("parent", $parent);
		$this->db->where("level", $level);
		$this->db->order_by("order", "asc");
		$menu = $this->db->get("menu");
		return $menu;
	}

	function get_by_level($level='1'){
		$this->db->where("level", $level);
		$this->db->order_by("order", "asc");
		$menu = $this->db->get("menu");
		return $menu;
	}

	function get_top(){
		$this->db->where("parent", '0');
		$this->db->where("level", '1');
		$this->db->order_by("order", "asc");
		$menu = $this->db->get("menu");
		return $menu;
	}

	function get_side($id = ''){
		if($id != ''){
			$level1 = $this->get($id);
		}else{
			$level1 = $this->get(0);
		}
		$menu = array();
		foreach ($level1->result() as $value1) {

			$level2 = $this->get_by($value1->id_menu, "2");
			foreach ($level2->result() as $value2){
				$slug = '';
				$level3 = $this->get_by($value2->id_menu, "3");
				if($level3->num_rows() > 0){
					foreach ($level3->result() as $value3) {
						$slug = $value1->slug."/".$value2->slug."/".$value3->slug;
						$menu[$value2->name."|".$value2->slug][$slug] = $value3->name;
					}
				}else{
					$slug = $value1->slug."/".$value2->slug;
					$menu[$slug] = $value2->name;
				}
			}
		}

		return $menu;
	}

	function get_breadcrumb(){
		
	}	

}

/* End of file menu_model.php */
/* Location: ./application/models/menu_model.php */

?>