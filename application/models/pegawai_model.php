<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pegawai_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){			

			$this->db->where("id_pegawai", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("vw_pegawai");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("vw_pegawai");
		return $data;
	}

	function insert(){
		$kode_pegawai 			= $this->input->post("kode_pegawai");
		$id_fakultas    		= $this->input->post("id_fakultas");
		$id_jurusan    			= $this->input->post("id_jurusan");
		$nama_pegawai 		  	= $this->input->post("nama_pegawai");
		$ketua_pegawai        	= $this->input->post("ketua_pegawai");
		$sk   					= $this->input->post("sk");


		$this->db->set("kode_pegawai", $kode_pegawai);
		$this->db->set("id_fakultas", $id_fakultas);
		$this->db->set("id_jurusan", $id_jurusan);
		$this->db->set("nama_pegawai", $nama_pegawai);
		$this->db->set("ketua_pegawai", $ketua_pegawai);
		$this->db->set("sk", $sk);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);

		$this->db->insert("pegawai");
		return $this->db->insert_id();
	}

	function update(){
		$id_pegawai 				= $this->input->post("id_pegawai");
		$kode_pegawai 			= $this->input->post("kode_pegawai");
		$id_fakultas    		= $this->input->post("id_fakultas");
		$id_jurusan    			= $this->input->post("id_jurusan");
		$nama_pegawai 		  	= $this->input->post("nama_pegawai");
		$ketua_pegawai        	= $this->input->post("ketua_pegawai");
		$sk   					= $this->input->post("sk");


		$this->db->set("kode_pegawai", $kode_pegawai);
		$this->db->set("id_fakultas", $id_fakultas);
		$this->db->set("id_jurusan", $id_jurusan);
		$this->db->set("nama_pegawai", $nama_pegawai);
		$this->db->set("ketua_pegawai", $ketua_pegawai);
		$this->db->set("sk", $sk);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);

		$this->db->where("id_pegawai", $id_pegawai);
		$this->db->update("pegawai");
		
	}

	function delete($id){
		$this->db->where("id_pegawai", $id);
		$this->db->delete("pegawai");
	}

}

/* End of file pegawai_model.php */
/* Location: ./application/models/pegawai_model.php */

?>