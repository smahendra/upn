<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laboratorium_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_laboratorium", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("vw_ak_lab");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("vw_ak_lab");
		return $data;
	}

	function insert(){
		$nama_laboratorium 	= $this->input->post("nama_laboratorium");
		$ruang     			= $this->input->post("ruang");
		$ketua 				= $this->input->post("ketua");

		$this->db->set("nama_laboratorium", $nama_laboratorium);
		$this->db->set("ruang", $ruang);
		$this->db->set("ketua", $ketua);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("ak_laboratorium");
		return $this->db->insert_id();
	}

	function update(){
		$nama_laboratorium 	= $this->input->post("nama_laboratorium");
		$ruang     			= $this->input->post("ruang");
		$ketua 				= $this->input->post("ketua");
		$id_laboratorium 	= $this->input->post("id_laboratorium");

		$this->db->set("nama_laboratorium", $nama_laboratorium);
		$this->db->set("ruang", $ruang);
		$this->db->set("ketua", $ketua);

		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_laboratorium", $id_laboratorium);
		$this->db->update("ak_laboratorium");		
	}

	function delete($id){
		$this->db->where("id_laboratorium", $id);
		$this->db->delete("ak_laboratorium");
	}

}

/* End of file laboratorium_model.php */
/* Location: ./application/models/laboratorium_model.php */

?>