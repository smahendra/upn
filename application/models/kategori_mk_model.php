<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_mk_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_kategori_mk", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("kategori_mk");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("kategori_mk");
		return $data;
	}

	function insert(){
		$nama_kategori 	= $this->input->post("nama_kategori");
		$singkatan     	= $this->input->post("singkatan");
		$keterangan 	= $this->input->post("keterangan");

		$this->db->set("nama_kategori", $nama_kategori);
		$this->db->set("singkatan", $singkatan);
		$this->db->set("keterangan", $keterangan);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("kategori_mk");
		return $this->db->insert_id();
	}

	function update(){
		$id_kategori_mk  	= $this->input->post("id_kategori_mk");
		$nama_kategori 		= $this->input->post("nama_kategori");
		$singkatan     		= $this->input->post("singkatan");
		$keterangan 		= $this->input->post("keterangan");

		$this->db->set("nama_kategori", $nama_kategori);
		$this->db->set("singkatan", $singkatan);
		$this->db->set("keterangan", $keterangan);
		$this->db->where("id_kategori_mk", $id_kategori_mk);
		$this->db->set("modified_by", '0');
		$this->db->set("modified", "now()", false);

		$this->db->update("kategori_mk");		
	}

	function delete($id){
		$this->db->where("id_kategori_mk", $id);
		$this->db->delete("kategori_mk");
	}

}

/* End of file kategori_mk_model.php */
/* Location: ./application/models/kategori_mk_model.php */

?>