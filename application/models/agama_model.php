<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agama_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_agama", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("agama");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("agama");
		return $data;
	}

	function insert(){
		$nama_agama 	= $this->input->post("nama_agama");

		$this->db->set("nama_agama", $nama_agama);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("agama");
		return $this->db->insert_id();
	}

	function update(){
		$nama_agama 	= $this->input->post("nama_agama");
		$id_agama 		= $this->input->post("id_agama");

		$this->db->set("nama_agama", $nama_agama);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_agama", $id_agama);
		$this->db->update("agama");
		
	}

	function delete($id){
		$this->db->where("id_agama", $id);
		$this->db->delete("agama");
	}

}

/* End of file fakultas_model.php */
/* Location: ./application/models/fakultas_model.php */

?>