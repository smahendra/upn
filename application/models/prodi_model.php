<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prodi_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){			

			$this->db->where("id_prodi", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("vw_prodi");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("vw_prodi");
		return $data;
	}

	function insert(){
		$kode_prodi 			= $this->input->post("kode_prodi");
		$id_jurusan    			= $this->input->post("id_jurusan");
		$nama_prodi 		  	= $this->input->post("nama_prodi");
		$ketua_prodi        	= $this->input->post("ketua_prodi");
		$sk   					= $this->input->post("sk");
		$sk_ijin   				= $this->input->post("sk_ijin");

		$this->db->set("kode_prodi", $kode_prodi);
		$this->db->set("id_jurusan", $id_jurusan);
		$this->db->set("nama_prodi", $nama_prodi);
		$this->db->set("ketua_prodi", $ketua_prodi);
		$this->db->set("sk", $sk);
		$this->db->set("sk_ijin", $sk_ijin);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("prodi");
		return $this->db->insert_id();
	}

	function update(){
		$id_prodi 				= $this->input->post("id_prodi");
		$kode_prodi 			= $this->input->post("kode_prodi");
		$id_jurusan    			= $this->input->post("id_jurusan");
		$nama_prodi 		  	= $this->input->post("nama_prodi");
		$ketua_prodi        	= $this->input->post("ketua_prodi");
		$sk   					= $this->input->post("sk");
		$sk_ijin   				= $this->input->post("sk_ijin");
		$id_prodi   			= $this->input->post("id_prodi");

		$this->db->set("kode_prodi", $kode_prodi);
		$this->db->set("id_jurusan", $id_jurusan);
		$this->db->set("nama_prodi", $nama_prodi);
		$this->db->set("ketua_prodi", $ketua_prodi);
		$this->db->set("sk", $sk);
		$this->db->set("sk_ijin", $sk_ijin);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_prodi", $id_prodi);
		$this->db->update("prodi");		
	}

	function delete($id){
		$this->db->where("id_prodi", $id);
		$this->db->delete("prodi");
	}

}

/* End of file program_studi_model.php */
/* Location: ./application/models/program_studi_model.php */

?>