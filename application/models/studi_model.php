<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Studi_model extends CI_Model {

	function get($id){
		
	}

	function get_all(){
		
	}

	function get_rancang_studi($id_mahasiswa='', $tahun_ajaran=''){
		if ($id_mahasiswa=='') {
			$id_mahasiswa = $this->input->post("id_mahasiswa");	
		}
		if ($tahun_ajaran=='') {
			$tahun_ajaran     = $this->input->post("tahun_ajaran");
		}
	
		$this->db->where("id_tahun_ajaran", $tahun_ajaran);
		$this->db->where("id_mahasiswa", $id_mahasiswa);
		$data = $this->db->get("vw_studi");
		
		return $data;
	}

	function get_max_sks($id_mahasiswa='', $semester=''){
		if ($id_mahasiswa=='') {
			$id_mahasiswa = $this->input->post("id_mahasiswa");	
		}
		if ($semester=='') {
			$semester     = $this->input->post("semester");
		}
		if($semester > 1){
			$last_semester = $semester-1;
		}else{
			$last_semester = $semester;
		}

		if ($semester == 1) {
			$max_sks = 24;
		}else{
			//$ip = $this->get_ip_semester($id_mahasiswa, $last_semester);
			$max_sks = 24;
		}

		return $max_sks;
	}

	function get_ip_semester($id_mahasiswa='', $semester=''){
		if ($id_mahasiswa=='') {
			$id_mahasiswa = $this->input->post("id_mahasiswa");	
		}
		if ($semester=='') {
			$semester     = $this->input->post("semester");
		}

		$this->db->select_sum("sks");
		$this->db->select_sum("index_nilai");
		$this->db->where("semester_mahasiswa", $semester);
		$this->db->where("id_mahasiswa", $id_mahasiswa);
		$data = $this->db->get("vw_studi")->row();
		$ip = $data->index_nilai / $data->sks;

		return $ip;
	}

	function cek_syarat_mk($syarat, $id){
		$this->db->where("id_mahasiswa", $id);
		$this->db->where("id_mata_kuliah", $syarat);
		$data = $this->db->get("vw_studi");
		if($data->num_rows() > 0){
			if($data->row()->nilai_angka >= 1){
				return 1;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	function cek_syarat_sks($syarat, $id){
		$this->db->select_sum("sks");
		$this->db->where("id_mahasiswa", $id);
		$data = $this->db->get("vw_studi")->row();
		if($syarat <= $data->sks){
			return 1;
		}else{
			return 0;
		}	
	}

	function insert($mahasiswa, $studi_list, $tahun_ajaran){
		$this->db->where("id_mahasiswa", $mahasiswa->id_mahasiswa);
		$this->db->where("semester_mahasiswa", $mahasiswa->semester);
		$this->db->where("id_tahun_ajaran", $tahun_ajaran);
		$this->db->delete("ak_trx_studi");
		foreach ($studi_list->result() as $studi) {
			$this->db->set("id_mata_kuliah", $studi->id_mata_kuliah);
			$this->db->set("id_mahasiswa", $mahasiswa->id_mahasiswa);
			$this->db->set("semester_mahasiswa", $mahasiswa->semester);
			$this->db->set("id_tahun_ajaran", $tahun_ajaran);
			$this->db->set("semester_mk", $studi->semester);
			$this->db->set("sks", $studi->sks);
			$this->db->set("created", "now()", false);
			$this->db->set("created_by", 0);
			$this->db->set("modified", "now()", false);
			$this->db->set("modified_by", 0);
			$this->db->insert("ak_trx_studi");
		}
	}

	function delete($id){
		
	}

}

/* End of file studi_model.php */
/* Location: ./application/models/studi_model.php */
?>