<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_user", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("user");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("user");
		return $data;
	}

	function insert(){
		$kode_user = $this->input->post("kode_user");
		$singkatan     = $this->input->post("singkatan");
		$nama_user = $this->input->post("nama_user");
		$sk            = $this->input->post("sk");
		$dekan         = $this->input->post("dekan");
		$pd1           = $this->input->post("pd1");
		$pd2           = $this->input->post("pd2");
		$pd3           = $this->input->post("pd3");
		$kurikulum     = $this->input->post("kurikulum");

		$this->db->set("kode_user", $kode_user);
		$this->db->set("singkatan", $singkatan);
		$this->db->set("nama_user", $nama_user);
		$this->db->set("sk", $sk);
		$this->db->set("dekan", $dekan);
		$this->db->set("pd1", $pd1);
		$this->db->set("pd2", $pd2);
		$this->db->set("pd3", $pd3);
		$this->db->set("kurikulum", $kurikulum);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);

		$this->db->insert("user");
		return $this->db->insert_id();
	}

	function update(){
		$id_user   = $this->input->post("id_user");
		$kode_user = $this->input->post("kode_user");
		$singkatan     = $this->input->post("singkatan");
		$nama_user = $this->input->post("nama_user");
		$sk            = $this->input->post("sk");
		$dekan         = $this->input->post("dekan");
		$pd1           = $this->input->post("pd1");
		$pd2           = $this->input->post("pd2");
		$pd3           = $this->input->post("pd3");
		$kurikulum     = $this->input->post("kurikulum");

		$this->db->set("kode_user", $kode_user);
		$this->db->set("singkatan", $singkatan);
		$this->db->set("nama_user", $nama_user);
		$this->db->set("sk", $sk);
		$this->db->set("dekan", $dekan);
		$this->db->set("pd1", $pd1);
		$this->db->set("pd2", $pd2);
		$this->db->set("pd3", $pd3);
		$this->db->set("kurikulum", $kurikulum);
		$this->db->where("id_user", $id_user);
		$this->db->update("user");
		
	}

	function delete($id){
		$this->db->where("id_user", $id);
		$this->db->delete("user");
	}

}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */

?>