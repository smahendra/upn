<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jurusan_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){			

			$this->db->where("id_jurusan", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("vw_jurusan");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("vw_jurusan");
		return $data;
	}

	function insert(){
		$kode_jurusan 			= $this->input->post("kode_jurusan");
		$id_fakultas    		= $this->input->post("id_fakultas");
		$nama_jurusan 		  	= $this->input->post("nama_jurusan");
		$ketua_jurusan        	= $this->input->post("ketua_jurusan");
		$sekretaris_jurusan   	= $this->input->post("sekretaris_jurusan");

		$this->db->set("kode_jurusan", $kode_jurusan);
		$this->db->set("id_fakultas", $id_fakultas);
		$this->db->set("nama_jurusan", $nama_jurusan);
		$this->db->set("ketua_jurusan", $ketua_jurusan);
		$this->db->set("sekretaris_jurusan", $sekretaris_jurusan);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("jurusan");
		return $this->db->insert_id();
	}

	function update(){
		$id_jurusan   			= $this->input->post("id_jurusan");
		$kode_jurusan 			= $this->input->post("kode_jurusan");
		$id_fakultas    		= $this->input->post("id_fakultas");
		$nama_jurusan 		  	= $this->input->post("nama_jurusan");
		$ketua_jurusan        	= $this->input->post("ketua_jurusan");
		$sekretaris_jurusan   	= $this->input->post("sekretaris_jurusan");

		$this->db->set("kode_jurusan", $kode_jurusan);
		$this->db->set("id_fakultas", $id_fakultas);
		$this->db->set("nama_jurusan", $nama_jurusan);
		$this->db->set("ketua_jurusan", $ketua_jurusan);
		$this->db->set("sekretaris_jurusan", $sekretaris_jurusan);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_jurusan", $id_jurusan);
		$this->db->update("jurusan");
		
	}

	function delete($id){
		$this->db->where("id_jurusan", $id);
		$this->db->delete("jurusan");
	}

}

/* End of file jurusan_model.php */
/* Location: ./application/models/jurusan_model.php */

?>