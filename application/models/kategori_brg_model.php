<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_brg_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_kategori_brg", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("inv_mtr_kategori_brg");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("inv_mtr_kategori_brg");
		return $data;
	}

	function insert(){
		$kode	   		= $this->input->post("kode");
		$nama_kategori 	= $this->input->post("nama_kategori");		
		$keterangan 	= $this->input->post("keterangan");

		$this->db->set("kode", $kode);
		$this->db->set("nama_kategori", $nama_kategori);
		$this->db->set("keterangan", $keterangan);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("inv_mtr_kategori_brg");
		return $this->db->insert_id();
	}

	function update(){
		$id_kategori_brg  	= $this->input->post("id_kategori_brg");
		$kode	   			= $this->input->post("kode");
		$nama_kategori 		= $this->input->post("nama_kategori");
		$keterangan 		= $this->input->post("keterangan");

		$this->db->set("kode", $kode);
		$this->db->set("nama_kategori", $nama_kategori);
		$this->db->set("keterangan", $keterangan);
		$this->db->where("id_kategori_brg", $id_kategori_brg);
		$this->db->set("modified_by", '0');
		$this->db->set("modified", "now()", false);

		$this->db->update("inv_mtr_kategori_brg");		
	}

	function delete($id){
		$this->db->where("id_kategori_brg", $id);
		$this->db->delete("inv_mtr_kategori_brg");
	}

}

/* End of file kategori_brg_model.php */
/* Location: ./application/models/kategori_brg_model.php */

?>