<?php
class XM_Controller extends CI_Controller {
    
    function template_view($content, $data=array(), $template="template/template-backend"){
    	$this->load->model("menu_model", "menu");
		$data['active']    = $this->system;
		$data['content']   = $content;
		$data['top_menu']  = $this->menu->get_top();
		$data['side_menu'] = $this->menu->get_side($this->system);
		//$this->output->cache(1);
		$this->load->view($template, $data);
	}

	function content_view($content, $data=array(), $template="template/template-backend"){
    	$data['content']   = $content;
		//$this->output->cache(1);
		$this->load->view($content, $data);
	}

	function modal_view($content, $data=array(), $template="template/template-modal"){
    	$data['content']   = $content;
		$this->load->view($template, $data);
	}
}
?>
