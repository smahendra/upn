<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends XM_Controller {

	var $system = 'superadmin';

	public function __construct(){
		parent::__construct();
		$this->load->model("user_model", "user");
		$this->load->model("menu_model", "menu");
	}

	public function index()
	{
		$data['page_title']       = "User Undiknas";
		$data['page_description'] = "Manajemen data user akses sistem undiknas";
		$data['list_data']		  = $this->user->get_all();
		$this->template_view('superadmin/user/main', $data);
	}

	function table(){
		$data['list_data']		  = $this->user->get_all();
		$this->load->view("superadmin/user/table", $data);
	}

	function detail($id){
		$data['data']		  = $this->user->get($id);
		$this->load->view("superadmin/user/detail", $data);
	}

	function insert(){
		$data['level1'] = $this->menu->get_by_level(1);
		$this->modal_view("superadmin/user/insert", $data);
	}

	function preset(){
		$data['menu'] = $this->menu->get_all();
		$this->load->view("superadmin/user/preset", $data);
	}

	function do_insert(){
		$this->user->insert();
		$this->table();
	}

	function edit($id){
		$data['data']		  = $this->user->get($id);
		$this->load->view("superadmin/user/edit", $data);
	}

	function do_edit(){
		$this->user->update();
		$this->table();
	}

	function do_delete($id){
		$this->user->delete($id);
		$this->table();
	}
}