<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_brg extends XM_Controller {

	var $system = 'inventori';

	public function __construct(){
		parent::__construct();
		$this->load->model("kategori_brg_model", "kategori");
	}

	public function index()
	{
		$data['page_title']       = "Master Kategori Barang";
		$data['page_description'] = "Manajemen kategori barang pada sistem $this->system";
		$data['list_data']		  = $this->kategori->get_all();
		$this->template_view('inventori/kategori_brg/main', $data);
	}

	function table(){
		$data['list_data']		= $this->kategori->get_all();
		$this->load->view("inventori/kategori_brg/table", $data);
	}

	function insert(){
		$this->modal_view("inventori/kategori_brg/insert");
	}

	function do_insert(){
		$this->kategori->insert();
		$this->table();
	}

	function edit($id){
		$data['data']		  = $this->kategori->get($id);
		$this->modal_view("inventori/kategori_brg/edit", $data);
	}

	function do_edit(){
		$this->kategori->update();
		$this->table();
	}

	function do_delete($id){
		$this->kategori->delete($id);
		$this->table();
	}

	function doPDF()
	{

		$tgl 			 	= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");
		$data['list_data'] 	= $this->kategori->get_all();
		
		//passing view ke variable dengan set param ke 3 true
	  	$html = $this->load->view("inventori/kategori_brg/pdf", $data, TRUE);
	  
	  	/* 
	  	export PDF menggunakan library mpdf
	  	library ada di folder aplikasi/libraries/mpdf/
	  	dokumentasi library ini ada di http://mpdf1.com/manual/
	  	*/
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf8','A4','','',10,10,10,10,10,10);
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output('Report Ketegori Barang '.$tgl.'.pdf','I');
		//========================================================
	}	

	function doExcel()
	{
		$tgl 		= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");
		$data 		= $this->kategori->get_all();

		$this->load->helper('html'); //libraries phpExcel ini memerlukan helper html
		/*
		export excel menggunakan library phpExcel
		library ini ada pada file aplikasi/libraries/phpExcel.php
		dokumentasi atau contoh library ini ada di folder aplikasi/libraries/phpExcel/documentation/
		*/
		$this->load->library('PHPExcel/IOFactory');
        $this->load->library('PHPExcel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setTitle("Undiknas University")
		                 ->setDescription("Data Kategori Barang Undiknas");
		                        
		// Assign cell values
		$objPHPExcel->setActiveSheetIndex(0);

		// header
		$objPHPExcel->getActiveSheet()->mergeCells("A1:E3");
		$objPHPExcel->getActiveSheet()->setCellValue('A1', "Data Kategori Barang di Undiknas");
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		//===================================================================

		// column header
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Kode Kategori');
		$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Nama Kategori');
		$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Keterangan');
		$objPHPExcel->getActiveSheet()->setCellValue('D4', 'created');
		$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Modified');
		//===================================================================

		$objPHPExcel->getActiveSheet()->getStyle('A4:E4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A4:E4')->getFill()->getStartColor()->setARGB('DDDDDDDD');
		$objPHPExcel->getActiveSheet()->getStyle('A1:E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:E4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:E4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

		//======= data row	
		$row = 4;
		foreach ($data->result() as $r) {
			$row++;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r->kode);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $r->nama_kategori);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $r->keterangan);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $this->xm->format_tanggal($r->created));
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $this->xm->format_tanggal($r->modified));
		}
		//===================================================================================

		$styleArray = array(
	             'allborders' => array(
	                 'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
	             )
	         );
			$objPHPExcel->getActiveSheet()->getStyle('A4:E'.$row)->applyFromArray($styleArray);

		// Save it as an excel 2003 file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="data kategori barang undiknas '.$tgl.'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}	
}