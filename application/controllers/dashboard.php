<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends XM_Controller {

	var $system = '';

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$data['page_title']       = "Dashboard";
		$data['page_active']       = "Dashboard";
		$data['page_description'] = "Undiknas Admin Panel dashboard";
		$this->template_view('template/dashboard', $data);
	}
}