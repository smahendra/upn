<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Khs extends XM_Controller {

	var $system = 'akademik';

	public function __construct(){
		parent::__construct();
		$this->load->model("studi_model", "studi");
		$this->load->model("mahasiswa_model", "mahasiswa");
		$this->load->model("fakultas_model", "fakultas");
		$this->load->model("jurusan_model", "jurusan");

		$this->load->model("mata_kuliah_model", "mk");
	}

	public function tambah_khs()
	{
		$data['page_title']       = "Kartu Hasil Studi";
		$data['page_description'] = "Halaman input nilai studi mahasiswa per semester";
		$data['fakultas'] 	  	  = $this->fakultas->get_all();		
		//$data['jurusan'] 	  	  = $this->jurusan->get_all();		
		$this->template_view('akademik/khs/main', $data);
	}

	function do_select_jurusan($id){
		$data = $this->mahasiswa->get_all_by_jurusan($id);
		echo json_encode($data->result_array());
	}
}