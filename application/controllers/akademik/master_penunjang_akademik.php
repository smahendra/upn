<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_penunjang_akademik extends XM_Controller {

	var $system = 'akademik';

	public function __construct(){
		parent::__construct();
		$this->load->model("Master_penunjang_akademik_model", "penunjang");
		$this->load->model("Laboratorium_model", "lab");		
	}

	public function index()
	{
		$data['page_title']       = "Master Penunjang Akademik";
		$data['page_description'] = "Manajemen data penunjang akademik pada sistem $this->system";
		$data['list_data']		  = $this->penunjang->get_all();
		$this->template_view('akademik/master_penunjang_akademik/main', $data);
	}

	function table(){
		$data['list_data']		  = $this->penunjang->get_all();
		$this->load->view("akademik/master_penunjang_akademik/table", $data);
	}

	function detail($id){
		$data['data']		  = $this->penunjang->get($id);
		$this->modal_view("akademik/master_penunjang_akademik/detail", $data);
	}

	function insert(){
		$data['laboratorium']		 = $this->lab->get_all();
		$this->modal_view("akademik/master_penunjang_akademik/insert",$data);
	}

	function do_insert(){
		$this->penunjang->insert();
		$this->table();
	}

	function edit($id){
		$data['laboratorium']		 = $this->lab->get_all();
		$data['data']		 		 = $this->penunjang->get($id);
		$this->modal_view("akademik/master_penunjang_akademik/edit", $data);
	}

	function do_edit(){
		$this->penunjang->update();
		$this->table();
	}

	function do_delete($id){
		$this->penunjang->delete($id);
		$this->table();
	}

	function doPDF()
	{
		$tgl 			 	= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");
		$data['list_data'] 	= $this->penunjang->get_all();
		
		//passing view ke variable dengan set param ke 3 true
	  	$html = $this->load->view("akademik/master_penunjang_akademik/pdf", $data, TRUE);
	  
	  	/* 
	  	export PDF menggunakan library mpdf
	  	library ada di folder aplikasi/libraries/mpdf/
	  	dokumentasi library ini ada di http://mpdf1.com/manual/
	  	*/
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf8','A4','','',10,10,10,10,10,10);
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output('Report master penunjang akademik '.$tgl.'.pdf','I');
		//========================================================
	}

	function doExcel(){
		$tgl 		= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");		
		$data 		  = $this->penunjang->get_all();

		$this->load->helper('html'); //libraries phpExcel ini memerlukan helper html
		/*
		export excel menggunakan library phpExcel
		library ini ada pada file aplikasi/libraries/phpExcel.php
		dokumentasi atau contoh library ini ada di folder aplikasi/libraries/phpExcel/documentation/
		*/
		$this->load->library('PHPExcel/IOFactory');
        $this->load->library('PHPExcel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setTitle("Master Penunjang Akademik Undiknas")
		                 ->setDescription("Data Master Penunjang Akademik Undiknas");
		                        
		// Assign cell values
		$objPHPExcel->setActiveSheetIndex(0);

		// header
		$objPHPExcel->getActiveSheet()->mergeCells("A1:D3");
		$objPHPExcel->getActiveSheet()->setCellValue('A1', "Data Master Penunjang Akademik di Undiknas");
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		//===================================================================

		// column header
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Nama');
		$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Laboratorium');
		$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Ruangan');
		$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Ketua Laboratorium');

		//===================================================================

		$objPHPExcel->getActiveSheet()->getStyle('A4:D4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A4:D4')->getFill()->getStartColor()->setARGB('DDDDDDDD');
		$objPHPExcel->getActiveSheet()->getStyle('A1:D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

		//======= data row	
		$row = 4;
		foreach ($data->result() as $r) {
			$row++;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r->nama);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $r->nama_laboratorium);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $r->nama_ruang);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $r->full_name);
		}
		//===================================================================================

		$styleArray = array(
	             'allborders' => array(
	                 'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
	             )
	         );
			$objPHPExcel->getActiveSheet()->getStyle('A4:D'.$row)->applyFromArray($styleArray);

		// Save it as an excel 2003 file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="data master penunjang akademik undiknas '.$tgl.'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
}