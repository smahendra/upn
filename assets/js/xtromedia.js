/*
Develop by Huma Prathama ~ xtromedia 2013
base on jquery 1.10.1 & Twitter bootstrap 2.3.2
metronic theme 1.1.4

semua plugin berdasarkan base CSS dan component dari bootstrap
jadi semua plugin ini hanya berjalan jika digabungkan dengan bootstrap dan tentunya jquery 1.9.1 serta metronic theme 1.1.2
*/

(function(jQuery){
	//plugin untuk membuat form biasa menjadi form ajax dan menampilkan hasil berupa modal atau portlet
	jQuery.fn.xmFormAjax = function(options){

		this.submit(function(e){
			e.preventDefault();
		  	var form          = jQuery(this);
			var formData      = form.serializeArray();
			var formAction    = form.attr("action");
			var $modal = $('#popup-modal');
			//$modal.modal('loading');
			if($('#success-action').attr('myattr') !== undefined) {
			    var successAction = form.attr("success-action"); //hide-modal, hide-portlet, show-modal, show-portlet
			}else{
			    var successAction = "hide-modal"; // default action
			}
			$modal.modal('loading');
	      	jQuery.ajax({
	        	url:formAction,
		        type:"post",
		        data:formData,
		        success:function(result){
					if(successAction == 'hide-modal') {
						jQuery(".table-container").html(result);
						jQuery(".modal").modal("hide");
						statusNote("Data tersimpan!");
					}else if(successAction == 'hide-portlet'){
						//buat action jika portlet dihilangkan
					}else if(successAction == 'show-portlet'){
						//action jika portlet ditampilkan
					}else if(successAction == 'show-modal'){
						//action jika modal dihilangkan
					}
					jQuery(".modal .modal-body").html("");
					xmTableInit();
		        },
		        error:function(res){
		          	alert("error");
		        }
	      	});
		});
	}


	jQuery.fn.xmFormAjaxPortlet = function(options){
		if($("#portlet-krs").length == 0){
			$("#main-portlet").after("<div class='row-fluid' id='portlet-krs'></div>");
		}
		this.submit(function(e){
			e.preventDefault();
			$("#initiate-loading-page").show();
			var form          = jQuery(this);
			var formData      = form.serializeArray();
			var formAction    = form.attr("action");
			var cek = true;
			jQuery.each(formData, function(index, val) {
				 if(val.value==''){
				 	cek = false;
				 }
			});
			if(cek){
				jQuery.ajax({
		        	url:formAction,
			        type:"post",
			        data:formData,
			        success:function(result){
			        	$("#portlet-krs").html(result);
			        	$("#initiate-loading-page").hide();
			        	cekMKMahasiswaChecked();
			        },
			        error:function(res){
			          	alert("form ajax portlet error");
			          	$("#initiate-loading-page").hide();
			        }
		      	});
			}else{
				$("#alert-modal .modal-body").html("<p>Silakan pilih data mahasiswa form pencarian dahulu!</p>");
				$("#alert-modal").modal("show");
				$("#initiate-loading-page").hide();
			}
		});
	}


	//plugin untuk membuat button atau anchor menampilkan hasil berupa modal menggunakan ajax
	jQuery.fn.xmShowModal = function(options){

		jQuery(this).click(function(e){
			e.preventDefault();
			$.fn.modalmanager.defaults.resize = true;
			$.fn.modalmanager.defaults.spinner = '<div class="loading-spinner fade" style="width: 200px; margin-left: -100px;"><img src="http://localhost/undiknas/assets/img/template-admin/ajax-modal-loading.gif" align="middle">&nbsp;<span style="font-weight:300; color: #eee; font-size: 18px; font-family:Open Sans;">&nbsp;Loading...</span></div>';
			
			$('body').modalmanager('loading');

			var url          	= jQuery(this).attr("href");
			var outputTrigger 	= jQuery(this).attr("data-original-title");
			var outputTitle  	= jQuery(this).attr("output-title");
			//var output       = jQuery(this).attr("output");
			//var portletPos   = jQuery(this).attr("portlet-position");
			//var portletUnder = jQuery(this).attr("portlet-position-under");
			var $modal = $('#popup-modal');

			if(outputTrigger == 'delete'){
				//var answer = confirm("Anda yakin ingin menghapus "+outputTitle+" ?");
				$("#confirm-modal .modal-body").html("<p>Anda yakin ingin menghapus "+outputTitle+" ?</p>");
				$("#confirm-modal").modal("show");
				$("#confirm-modal #confirm-delete").click(function(){
					$("#confirm-modal").modal('loading');
					jQuery.ajax({
						url:url,
						type:"POST",
						success:function(result){
							jQuery(".table-container").html(result);
							xmTableInit();
							statusNote("Data terhapus!");
							$("#confirm-modal").modal("hide");
						},
						error:function(){
							alert("something error! contact your developer...");
						}
					});
				});	
			}else{
				$modal.load(url, '', function(response, status, xhr){
					if (status == "error") {
					    var msg = "maaf, halaman error : ";
					    alert(msg + xhr.status + " " + xhr.statusText);
				  	}else{
				  		jQuery("#popup-modal .modal-header h3").html(outputTitle);
			      		$modal.modal().on("hidden", function() {
	              			$modal.empty();
	              		});
	              		xmFormInit();
				  	}
			    });
			}
		});
	}

	//plugin untuk membuat button atau anchor menampilkan hasil berupa portlet menggunakan ajax
	jQuery.fn.xmShowPortlet = function(options){
		jQuery(this).click(function(e){
			e.preventDefault();
			//disabledScreen();
			var url = jQuery(this).attr("href");
			var elementTitle = jQuery(this).attr("special");
			var title = jQuery(this).attr("modal-title");
			
			jQuery.ajax({
				url:url,
				type:"POST",
				success:function(result){
					jQuery(".modal .modal-header h3").html(title);
					jQuery(".modal .modal-body").html(result);
					jQuery('.modal').modal("show").css({'width': '1000px','margin-left': function () {return -(jQuery(this).width() / 2);}});
				},
				error:function(){
					alert("something error! contact your developer...");
				}
			});
			
		});
	}

	jQuery.fn.xmConcatOnChange = function(options){
		jQuery(this).change(function(e){
			var elemen             = jQuery(this).attr("elemen-affected");
			var elemen_placeholder = jQuery(elemen).attr("data-placeholder");
			var url                = jQuery(this).attr("data-url");
			var concat_text        = jQuery(this).attr("concat-text");
			var concat_text2       = jQuery(this).attr("concat-text2");
			var concat_value       = jQuery(this).attr("concat-value");
			var id                 = jQuery(this).val();
			
			jQuery.ajax({
				url:url+'/'+id,
				type:"POST",
				dataType:"json",
				success:function(json){
					jQuery("#id_mahasiswa").html("");
					if(json.length > 0){
						jQuery(elemen).append(jQuery('<option>').text(elemen_placeholder).attr('value', 0));
						jQuery.each(json, function(i, value) {
						if (concat_text2 != '') {
							var option_text = json[i][concat_text]+' - '+json[i][concat_text2];
						}else{
							var option_text = json[i][concat_text];
						}
            			jQuery(elemen).append(jQuery('<option>').text(option_text).attr('value', json[i][concat_value]));
            				jQuery(elemen).trigger("liszt:updated");
            			});
					}else{
						jQuery(elemen).trigger("liszt:updated");
					}
        		},
				error:function(){
					alert("something error! contact your developer...");
				}
			});
			
		});
	}	

	jQuery.fn.isExist = function(){
        return jQuery(this).length > 0;
 	}

 	jQuery.fn.dataTableExt.oApi.showModal = function (oSettings) {
    	if(jQuery(".show-modal").isExist()){
			jQuery(".show-modal").xmShowModal();
		}
	};
	
})(jQuery);	

function statusNote(msg){
	$("#first-content").before('<div id="ajax-status-note" class="alert alert-error fade in hide">'+msg+'<button type="button" class="close" data-dismiss="alert"></button></div>');
	$("#ajax-status-note").slideDown("slow",function(){
		setTimeout(function(){
			$("#ajax-status-note").slideUp("slow");
			$("#ajax-status-note").remove();
		}, 3000);
	});
}


function xmPluginInit(){
	xmFormInit();
	xmTableInit();
}

function xmFormInit(){
	if(jQuery("form.ajax-handler").isExist()){
		$("form.ajax-handler").xmFormAjax();
	}
	if(jQuery(".validate-form").isExist()){
		$(".validate-form").validate();
	}
	if(jQuery(".chosen").isExist()){
		$(".chosen").chosen();
	}
	if(jQuery(".on-change").isExist()){
		$(".on-change").xmConcatOnChange();
	}
	if(jQuery("form.ajax-portlet").isExist()){
		jQuery("form.ajax-portlet").xmFormAjaxPortlet();;
	}
}

function xmTableInit(){
	if(jQuery(".data-tables-handler").isExist()){
		var dontSort = [];
                $('.data-tables-handler thead th').each( function () {
                    if ( $(this).hasClass( 'no-sort' )) {
                        dontSort.push( { "bSortable": false } );
                    } else {
                        dontSort.push( null );
                    }
                });    
		jQuery(".data-tables-handler").dataTable({
			"aLengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"] // change per page values here
                ],
			"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bStateSave": true,
            "aoColumns": dontSort,
            "fnDrawCallback": function(){
            	if(jQuery(".show-modal").isExist()){
					jQuery(".show-modal").xmShowModal();
				}
            },
			"oLanguage": {
			 	"sSearch": "",
			 	"sLengthMenu": "_MENU_ Data per halaman",
	            "sZeroRecords": "Tidak ada data ditemukan",
	            "sInfo": "Menampilkan _START_ sampai _END_ dari total _TOTAL_ data",
	            "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
	            "sInfoFiltered": "(pencarian dari _MAX_ total data)",
	            "oPaginate": {
                        "sPrevious": "Sebelumnya",
                        "sNext": "Selanjutnya"
                    }
			}
		});
		jQuery('.dataTables_filter input').attr("placeholder", "Cari...");
		jQuery('.dataTables_filter input').addClass("m-wrap small"); // modify table search input
        jQuery('.dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
        jQuery('.dataTables_length select').select2(); // initialzie select2 dropdown
	}
}

function cekMKChecked(){
	var values = $('input:checkbox:checked.checkmk').map(function () {
			  return this.value;
			}).get();
			$("#mklist").val(values);
}

function cekMKMahasiswaChecked(){
	var values = $('input:checkbox:checked.checkmhs').map(function () {
			  return this.value;
			}).get();
	var totalSKS = $('input:checkbox:checked.checkmhs').map(function () {
			  return $(this).attr("data-sks");
			}).get();
	$("#mkmahasiswa").val(values);
	var totalSKSMHS = 0;
	$.each(values, function(index, val) {
		$("#mk_list #tr"+val).ready(function(){
			$("#mk_list #tr"+val).hide();
		});
	});
	$.each(totalSKS, function(index, val) {
		totalSKSMHS = totalSKSMHS + parseInt(val);
	});		
	$("#total-sks").html(totalSKSMHS);
}

function cekMaxSks(element_sks){
	var max_sks = $("#max-sks").val();
	var tot = parseFloat(element_sks);
    $.each($("input:checkbox:checked.checkmhs"), function (k, v){
    	var val =parseFloat($(this).attr("data-sks"));
    	tot = parseFloat(tot + val);   
    });
        if (max_sks >= tot) {
        	return true;
        }else{
        	return false;
        }
}

function cekSyaratMk(syarat_mk, syarat_sks){
	var result;
		$.ajax({
			cache: false,
			async:false,
			url: '<?php echo site_url("akademik/rancang_studi/do_cek_syarat") ?>',
			type: 'POST',
			data: {syarat_mk: syarat_mk,
					syarat_sks: syarat_sks,
					id_mahasiswa: $("#id-mahasiswa").val()
					},
			success:function(res) {
				if(res=='0' || res==0){
					result = false;
				}else{
					result = true;
				}
			}		
		})
		.fail(function(res) {
			alert("some error");
		});
	return result;	
}